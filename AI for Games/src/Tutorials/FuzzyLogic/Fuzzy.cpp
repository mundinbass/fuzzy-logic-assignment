//#########################################
// This file was created by:			
// Marco Valdez Balderas	#:100527945	
// Aaron Macaulay			#:100492646	
// For AI for Games, Winter 2019.		
// Professor:	Dr. Fletcher Lu			
// TA:			David Arppe			    
// Date last edited: March 20, 2019	   
//#########################################

#include "Fuzzy.h"
#include "imgui.h"
#include "GLM/glm.hpp"

float TrapezoidFuzzy::getValue(float a_input)							   // getValue is the evaluation function
{																		   //
	if (a_input <= MinValue)											   // if input value is less than minimum;
		return 0;														   // it is not a member of this fuzzy set.
	else if (a_input < Left_Middle)										   // if input value is less than the left middle value,
		return (a_input - MinValue) / (Left_Middle - MinValue);			   //	return the percentage (0-1) of membership between minimum and the middle point
	else if (a_input <= Right_Middle)									   // if input value is greater than left middle point and less than right middle point,
		return 1.0;														   //	it is definitely a part of this est
	else if (a_input < MaxValue)										   // same thing as the percentage for left middle, just backwards
		return (MaxValue - a_input) / (MaxValue - Right_Middle);		   // ^
	else																   //
		return 0;														   // If its not in any of these, then it is definitely not in the set
}

void TrapezoidFuzzy::setMiddle(float L, float R)
{
	Left_Middle = L; Right_Middle = R;									// Set the middle (top) points of the trapezoid
}

float TriangleFuzzy::getValue(float a_input)								// 
{																			//
	if (a_input <= MinValue)												//
		return 0;															//
	else if (a_input < Middle)												//
		return (a_input - MinValue) / (Middle - MinValue);					// Same thing as the trapezoid, but no middle part essentially.
	else if (a_input == Middle)												// (no area (only a single point) where the value is definitely a part of the set)
		return 1.0;															//
	else if (a_input < MaxValue)											//
		return (MaxValue - a_input) / (MaxValue - Middle);					//
	else																	//
		return 0;
}

void TriangleFuzzy::setMiddle(float L, float R)
{
	Middle = L;																// Set middle point for triangle. R is not used 
}

void BaseFuzzy::setInterval(float leftval, float rightval)
{
	MinValue = leftval; MaxValue = rightval;								// Set the minimum value for membership and maximum value for membership
}

void BaseFuzzy::setType(char c)
{
	cType = c;																// cType is a way to keep track of which type (trapezoid, r;  or triangle, t)
}

void BaseFuzzy::setName(std::string a_name)
{
	sName = a_name;															// Set a name for the set
}

void BaseFuzzy::DrawGraph(float min, float max)								// A function to draw a graph 
{
	{
		float points[100];													//Sets amount of points
		for (int i = 0; i < 100; i++)
		{
			points[i] = this->getValue(glm::mix(min, max, i / 100.0f));		//Loop through each point, setting each point by getting the value from evaluation
		}

		ImGui::PlotLines(getName().c_str(), points, 100, 0, 0, 0.0f, 1.0f);	//Plot the lines on the graph
	}
}

char BaseFuzzy::getType() const
{
	return cType;									// Simple get function to return the type (t or r)
}

std::string BaseFuzzy::getName() const
{
	return sName;									// simple get function to get the name of the set
}