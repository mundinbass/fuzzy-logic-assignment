//#########################################
// This file was created by:			
// Marco Valdez Balderas	#:100527945	
// Aaron Macaulay			#:100492646	
// For AI for Games, Winter 2019.		
// Professor:	Dr. Fletcher Lu			
// TA:			David Arppe			    
// Date last edited: March 20, 2019	   
//#########################################

#pragma once
#include <GLM/glm.hpp>
#include <random>
#include <vector>
#include "Fuzzy.h"

class Wasp;

enum AntType				// Not used 
{
	SOLDIER,
	WORKER,
	COUNT
};

class GameObject																			   // Base object for all objects in game
{																							   //
public:																						   //
	GameObject() : pos(glm::vec2(0, 0)), size(glm::vec2(50, 50))							   // Constructor
	{}																						   //
	GameObject(glm::vec2 startPositon, glm::vec2 Size) : pos(startPositon), size(Size)		   //
	{}																						   //
	~GameObject()																			   //
	{}																						   //
	glm::vec2 pos;																			   // self explanatory
	glm::vec2 size;																			   // ditto ^
	glm::vec3 color;																		   // look here ^
	glm::vec2 direction;																	   // direction the object will move
	virtual void update(float a_deltatime);													   // update
};

class FoodSource : public GameObject														   // Food source object
{																							   // 
public:																						   // 
	FoodSource() : totalFood(10)															   // constructor. total food isnt really used
	{}																						   // 
	FoodSource(int foodAmount) : totalFood(foodAmount)										   // 
	{}																						   // 
																							   // 
	void GiveFood()																			   // When an ant gets food, take a food away from supply
	{																						   // 
		if (totalFood > 0) totalFood--;														   // 
	}																						   // 
																							   // 
	//void update(float a_deltatime) override;												   // 
	bool IsThereFood()																		   // self explanatory
	{																						   // 
		return totalFood > 0;																   // 
	}																						   // 
protected:																					   // 
	int totalFood;																			   // amount of food in the source "restaurant"
};																							   // 

class AntMine : public GameObject															   // Ant mine object. The ants work here.
{																							   // 
public:																						   // 
	AntMine()																				   // Constructor. defaults to 0 credits called Werk
	{																						   // 
		Werk = 0;																			   // 
	}																						   // 
	int Werk;																				   // Amount of credits the player has
	void update(float);																		   // Update
	int NumberOfWorkingAnts;																   // Number of ants that are currently in Mine state
	float timer = 0.0f;																		   // Timer used to earn credits only once per second
private:																					   // No secrets to see here.
};																							   

class Ant : public GameObject																   // Ant class. This is an important class.
{																							   //
public:																						   //
	enum States																				   // A bunch of states the ant can be in
	{																						   //
		GATHER,																				   //
		RETURN,																				   //
		WANDER,																				   //
		MINE,																				   //
		ATTACK,																				   //
		COUNT																				   //
	};																						   //
																							   //
	Ant()																					   // Constructor
	{																						   //
		currentState = MINE;																   //
		hunger = 40.0f;																		   //
		timer = 0.0f;																		   //
		hungerTimer = 0.0f;																	   //
	}																						   //
	Ant(glm::vec2 position)																	   // Another constructor
	{																						   //
		pos = position;																		   //
		hunger = 40.0f;																		   //
		timer = 0.0f;																		   //
		hungerTimer = 0.0f;																	   //
																							   //
	}																						   //
	~Ant()																					   // Destructor
	{}																						   //
																							   //
	void SetFoodSource(FoodSource &food)													   // Set the food source so the ant knows where the food is
	{																						   //
		foodSource = food;																	   //
	}																						   //
																							   //
	std::string name;																		   // Name of the ant. Generally an index, such as 1, 2, 3, etc
	AntType myType;																			   // Type of ant. Legacy; isn't used.
	AntType type();																			   // Get type function
	glm::vec2 homePosition;																	   // Position of the mine
	FoodSource foodSource;																	   // The food source, so the ant knows what it is 
	BaseFuzzy	*HungerSet[3];																   // A pointer to the fuzzy hunger set so it can decide whether its hungry or not
	BaseFuzzy	*SafetySet[2];																   // A pointer to the fuzzy safety set so it can decide whether its safe or not
	float			membershipRate[3];														   // Keeps track of "how much of a member" it is of the hunger set
	float			safetyRate[2];															   // same for safety set
	//Fuzzy needs;																			   //
	float hunger;								// 0 - 100, 0 is starving.					   // Hunger value
	float hungerTimer = 0.0f;																   // Used to calculate how much hunger to apply
	bool choose(float a_hunger);															   // 
	virtual void update(float a_deltatime) override;										   // update
	void update(float a_deltatime, std::vector<Wasp>& list);								   // update
	//float determined;																		   //
	States currentState;																	   // stores current state
	float health = 100;																		   // Health value
	float timer;																			   // General timer for the class. not used at this moment.
	float attackSpeed = 0.5f;																   // How fast the ant attacks
	void printState();																		   // for debug, std::cout some important values
	std::string getState();																	   // Get current state
	int waspSize;																			   // The size of the wasps array (how many wasps there are)
																							   //
	int waspIndex;																			   // Used to keep track of closest wasp
																							   //
};																							   //

class Wasp : public GameObject																   // Wasp object
{																							   //
	enum States																				   // Possible states
	{																						   //
		ROAM,																				   //
		ATTACK,																				   //
		THREATENED,																			   //
		COUNT																				   //
	};																						   //
public:																						   //
	Wasp();																					   //
	virtual void update(float a_deltatime) override;										   //
	void update(float a_deltatime, std::vector<Ant>& list);									   //
	float speed = 100;																		   // Speed the wasp flies/walks
	int antSize;																			   // number of ants that exist
	void CheckNearbyAnts(std::vector<Ant>& list);											   // Check if and which ants are nearby
	void DamageWasp(float damage)															   // A function used by ants to damage this wasp
	{																						   //
		health -= damage;																	   //
		currentState = ATTACK;																   //
	}																						   //
	float health = 100;																		   // Health value. once it reaches 0 the wasp dies
private:																					   //
	Ant targetAnt;																			   // The ant hte wasp wants to kill.
	BaseFuzzy	*HungerSet[3];																   // Wasps can be hungry. not used at the moment.
	States currentState;																	   // 
																							   //
	float timer;																			   //
	float directionTime = 1;															   // Every 1 second, the wasp chooses a new direction to go
	float attackSpeed = 1.f;																   //
																							   //
	int antIndex = 0;																		   // Index of closest ant in the ants array
};																							   //

static std::vector<Wasp> wasps;	// All the wasps. Master global array. Important
static std::vector<Ant> ants;	// All the ants. Master global array. Important
static AntMine antMine;			// The mine where the ants work