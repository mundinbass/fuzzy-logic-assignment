//#########################################
// This file was created on top of a base file
// given to students in tutorial by David Arppe.
// It was edited by:			
// Marco Valdez Balderas	#:100527945	
// Aaron Macaulay			#:100492646	
// For AI for Games, Winter 2019.		
// Professor:	Dr. Fletcher Lu			
// TA:			David Arppe			    
// Date last edited: March 20, 2019	   
//#########################################

#include <GL/gl3w.h>
#include <GLFW/glfw3.h> // GLFW helper library
#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp> // for glm::ortho

// IMGUI
#include <imgui.h>
#include <imgui_impl_glfw_gl3.h>

#include <iostream> // Used for 'cout'
#include <stdio.h>  // Used for 'printf'
#include <SOIL.h>

#include "Shaders.h"
#include "Pong.h"
#include "Ant.h"

/*---------------------------- Variables ----------------------------*/
// GLFW window
GLFWwindow* window;
int width = 1280;
int height = 720;

// Uniform locations
GLuint mvp_loc, col_loc;

// OpenGl stuff
GLuint shader_program;
GLuint quad_vbo; // vertex buffer object
GLuint quad_vao; // vertex array object

// Matrices
glm::mat4 modelMatrix;
glm::mat4 viewMatrix;
glm::mat4 projectionMatrix;

// Global variables
PongPaddle leftPaddle, rightPaddle;
PongBall ball(glm::vec2(0.0f), glm::vec2(1.0f, 1.0f));
Ant ant; 
//AntMine antMine;
FoodSource foodSupply = FoodSource(100);

int hillSize = 1;

int numOfWorkingAnts = 0;

Wasp wasp;

bool mouseClick = false, spawnAntClick = false;
// Functions
void DrawQuad(glm::vec2, glm::vec2, glm::vec3 = glm::vec3(1.0f));

BaseFuzzy *FuzzySet[3];
BaseFuzzy *SafetySet[2];
glm::vec3 viewToWorldCoordTransform(int x, int y);
void SetFuzzySet();
void SetSafetySet();

float timer = 0;
float FuzzyMin[3];
float FuzzyMax[3];

float SafetyMin[2];
float SafetyMax[2];

GLuint texture;
unsigned int dirtImage;

void SpawnWasp(float time);

void Initialize()
{
    // Create a shader for the lab
    GLuint vs = buildShader(GL_VERTEX_SHADER, ASSETS"primitive.vs");
    GLuint fs = buildShader(GL_FRAGMENT_SHADER, ASSETS"primitive.fs");
    shader_program = buildProgram(vs, fs, 0);
    dumpProgram(shader_program, "Pong shader program");

	dirtImage = SOIL_load_OGL_texture(ASSETS"Images/ant-512.png",
		SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT);

    // Create all 4 vertices of the quad
    glm::vec3 p0 = glm::vec3(-1.0f, -1.0f, 0.0f);
    glm::vec3 p1 = glm::vec3(-1.0f, 1.0f, 0.0f);
    glm::vec3 p2 = glm::vec3(1.0f, -1.0f, 0.0f);
    glm::vec3 p3 = glm::vec3(1.0f, 1.0f, 0.0f);
    // Create a list of vertices
    glm::vec3 vertices[12] =
    {
        // Bottom face
        p0, p1, p2, p3,
    };	
	
    glGenVertexArrays(1, &quad_vao);
    glBindVertexArray(quad_vao);

    glGenBuffers(1, &quad_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, quad_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), NULL, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);

    glUseProgram(shader_program);
    GLuint vPosition = glGetAttribLocation(shader_program, "vPosition");
    glVertexAttribPointer(vPosition, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(vPosition);


	mvp_loc = glGetUniformLocation(shader_program, "modelViewProjMat");
    col_loc = glGetUniformLocation(shader_program, "boxColor");

	glUniform1i(mvp_loc, 0);

	foodSupply.pos = glm::vec2(400, -100);

	antMine.pos = glm::vec2(-400, 200);
	antMine.size = glm::vec2(100, 100);

	wasp.color = glm::vec3(1, 1, 0);
	wasp.size = glm::vec2(25, 75);

	ant.size = glm::vec2(25, 25);
	ant.SetFoodSource(foodSupply);

	SetFuzzySet();
	SetSafetySet();

}

void Update(float a_deltaTime)
{
	system("cls");
    if (glfwGetKey(window, GLFW_KEY_ESCAPE))
        glfwSetWindowShouldClose(window, true);

	int state = glfwGetKey(window, GLFW_KEY_S);
	//Spawn ants
	if(state == GLFW_PRESS && !spawnAntClick)
	{
		spawnAntClick = true;
		Ant temp;
		temp = ant;
		temp.pos = antMine.pos;
		temp.homePosition = antMine.pos;
		temp.HungerSet[0] = FuzzySet[0];
		temp.HungerSet[1] = FuzzySet[1];
		temp.HungerSet[2] = FuzzySet[2];

		temp.SafetySet[0] = SafetySet[0];
		temp.SafetySet[1] = SafetySet[1];
		temp.name = std::to_string(ants.size() + 1);		
		ants.push_back(temp);
	}
	else if(state == GLFW_RELEASE && spawnAntClick)
	{
		spawnAntClick = false;
	}

	//Spawn wasps with awful mouse positioning
	double mouseX, mouseY;
	glfwGetCursorPos(window, &mouseX, &mouseY);

	int mouseState = glfwGetKey(window, GLFW_KEY_W);
	
	if (mouseState == GLFW_PRESS && !mouseClick)
	{
		mouseClick = true;

		glm::vec3 mousePos = viewToWorldCoordTransform(mouseX, mouseY);
		//Spawn wasp
		wasp.pos = glm::vec2(mousePos.x, mousePos.y);

		wasps.push_back(wasp);
	}
	else if (mouseState == GLFW_RELEASE && mouseClick)
		mouseClick = false;

	// UPDATE
	// Wasps
	//Update all current Wasps
	for(int i = 0; i < wasps.size(); i++)
	{
		wasps[i].pos = glm::clamp(wasps[i].pos, glm::vec2((-width / 2), (-height / 2)), glm::vec2((width / 2), (height / 2)));
		wasps[i].update(a_deltaTime, ants);
		if(wasps[i].health <= 0)
		{
			wasps.erase(wasps.begin() + i);
		}
	};

	//Spawn wasps consistently
	SpawnWasp(25);
	// Ants

	//Update all current ants
	for (int i = 0; i < ants.size(); i++)
	{
		ants[i].pos = glm::clamp(ants[i].pos, glm::vec2((-width / 2), (-height / 2)), glm::vec2((width / 2), (height / 2)));

		ants[i].HungerSet[0] = FuzzySet[0];
		ants[i].HungerSet[1] = FuzzySet[1];
		ants[i].HungerSet[2] = FuzzySet[2];

		ants[i].SafetySet[0] = SafetySet[0];
		ants[i].SafetySet[1] = SafetySet[1];


		ants[i].update(a_deltaTime, wasps);
		if(ants[i].health <= 0)
		{
			ants.erase(ants.begin() + i);
		}
	}
	// Mine
	antMine.update(a_deltaTime);

	// Check how many ants are working on the mine
	int tmp = 0;
	for (int i = 0; i < ants.size(); i++)
	{
		if (ants[i].currentState == Ant::States::MINE)
		{
			std::cout << "I went through the workers!\n";
			tmp++;
		}
	}
	//std::cout << "tmp:\t" << tmp << "\n";
	// Tell the mine how many ants are working at it
	antMine.NumberOfWorkingAnts = tmp;

	// Win condition
	if (antMine.Werk >= 1000)
	{
		std::cout << "You win!";
	}
	timer += a_deltaTime;
}

void Render()
{
    glUseProgram(shader_program);
    
    viewMatrix = glm::mat4(1.0f);
    projectionMatrix = glm::ortho(-640.0f, 640.0f, -360.0f, 360.0f, -1.0f, 1.0f);

	glActiveTexture(GL_TEXTURE0);

	//glBindTexture(GL_TEXTURE_2D, dirtImage);
	//DrawQuad(glm::vec2(0, 0), glm::vec2(75, 75));

	DrawQuad(foodSupply.pos, foodSupply.size, glm::vec3(1, 0, 0));
	DrawQuad(antMine.pos, antMine.size * (float)hillSize, glm::vec3(0, 1, 0));

	for(int i = 0; i < wasps.size(); i++)
	{
		DrawQuad(wasps[i].pos + glm::vec2(0, 50), glm::vec2((wasps[i].health / 100) * 70, 5), glm::vec3(1, 0, 0));
		DrawQuad(wasps[i].pos, wasps[i].size, wasps[i].color);
	}

	for(int i = 0; i < ants.size(); i++)
	{
		DrawQuad(ants[i].pos + glm::vec2(0, 35), glm::vec2((ants[i].health / 100) * 70, 5), glm::vec3(1, 0, 0));
		DrawQuad(ants[i].pos, ants[i].size);
	}

	//glBindTexture(GL_TEXTURE_2D, GL_NONE);
    glUseProgram(GL_NONE);
}

void GUI()
{
    ImGui::Begin("Settings", 0, ImVec2(100, 50), 0.4f);
    {
        // Show some basic stats in the settings window 
        ImGui::Text("%.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
    }
	ImGui::Text("You have %i credits to spend", antMine.Werk);

	if (antMine.Werk >= 100.0f * hillSize)
	{
		if (ImGui::Button("Buy an upgrade!")) 
		{
			antMine.Werk -= 100.0f;
			hillSize++;

			std::cout << "You have successfully purchased an upgrade.\n";
		}
	}

	ImGui::Text("Hunger Fuzzy Set");
	ImGui::SliderFloat3("Fuzzy Min Values", FuzzyMin, 0, 100);
	ImGui::SliderFloat3("Fuzzy Max Values", FuzzyMax, 0, 100);

	FuzzySet[0]->setInterval(FuzzyMin[0], FuzzyMax[0]);
	FuzzySet[1]->setInterval(FuzzyMin[1], FuzzyMax[1]);
	FuzzySet[2]->setInterval(FuzzyMin[2], FuzzyMax[2]);

	ImGui::Text("Safety Fuzzy Set");
	ImGui::SliderFloat2("Safety Min Values", SafetyMin, 0, 100);
	ImGui::SliderFloat2("Safety Max Values", SafetyMax, 0, 100);

	SafetySet[0]->setInterval(SafetyMin[0], SafetyMax[0]);
	SafetySet[1]->setInterval(SafetyMin[1], SafetyMax[1]);

    ImGui::End();
	ImGui::Begin("Graphs", 0, ImVec2(100, 50), 0.4f);
	{
		ImGui::TextColored(ImVec4(1, 0, 0, 1), "Hunger graph");
		FuzzySet[0]->DrawGraph(0, 100);
		FuzzySet[1]->DrawGraph(0, 100);
		FuzzySet[2]->DrawGraph(0, 100);

		ImGui::TextColored(ImVec4(0, 1, 0, 1), "Safety graph");
		SafetySet[0]->DrawGraph(0, 100);
		SafetySet[1]->DrawGraph(0, 100);
	}
	ImGui::End();
}

void Cleanup()
{
    glDeleteBuffers(1, &quad_vbo);
    glDeleteVertexArrays(1, &quad_vao);
    glDeleteProgram(shader_program);
}

void DrawQuad(glm::vec2 a_position, glm::vec2 a_size, glm::vec3 a_color)
{
    modelMatrix = glm::mat4(1.0f);
    modelMatrix = glm::translate(modelMatrix, glm::vec3(a_position.x, a_position.y, 0.0f));
    modelMatrix = glm::scale(modelMatrix, glm::vec3(a_size.x * 0.5f, a_size.y * 0.5f, 1.0f));

    glm::mat4 modelViewProjMat = projectionMatrix * viewMatrix * modelMatrix;

    glUniformMatrix4fv(mvp_loc, 1, 0, &modelViewProjMat[0][0]);
    glUniform3fv(col_loc, 1, &a_color[0]);

    glBindVertexArray(quad_vao);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

}

static void ResizeEvent(GLFWwindow* a_window, int a_width, int a_height)
{
    // Set the viewport incase the window size changed
    glfwGetFramebufferSize(window, &width, &height);
    glViewport(0, 0, width, height);
}

glm::vec3 viewToWorldCoordTransform(int mouse_x, int mouse_y)
{
	// NORMALISED DEVICE SPACE
	double x = 2.0 * mouse_x / width - 1;
	double y = 2.0 * mouse_y / height - 1;

	// HOMOGENEOUS SPACE
	glm::vec4 screenPos = glm::vec4(x, -y, -1.0f, 1.0f);
	
	// Projection/Eye Space
	glm::mat4 ProjectView = projectionMatrix * viewMatrix;
	glm::mat4 viewProjectionInverse = inverse(ProjectView);

	glm::vec4 worldPos = viewProjectionInverse * screenPos;
	return glm::vec3(worldPos);
}

void SetFuzzySet(){
	FuzzySet[0] = new TrapezoidFuzzy;
	FuzzySet[1] = new TriangleFuzzy;
	FuzzySet[2] = new TrapezoidFuzzy;

	FuzzyMin[0] = 0;
	FuzzyMax[0] = 30;

	FuzzyMin[1] = 35;
	FuzzyMax[1] = 45;

	FuzzyMin[2] = 40;
	FuzzyMax[2] = 100;

	FuzzySet[0]->setInterval(FuzzyMin[0], FuzzyMax[0]);
	FuzzySet[0]->setMiddle(0, 20);
	FuzzySet[0]->setType('r');
	FuzzySet[0]->setName("very_hungry");

	FuzzySet[1]->setInterval(FuzzyMin[1], FuzzyMax[1]);
	FuzzySet[1]->setMiddle(35, 35);
	FuzzySet[1]->setType('t');
	FuzzySet[1]->setName("comfortable");

	FuzzySet[2]->setInterval(FuzzyMin[2], FuzzyMax[2]);
	FuzzySet[2]->setMiddle(50, 100);
	FuzzySet[2]->setType('r');
	FuzzySet[2]->setName("full");
}

void SetSafetySet()
{
	SafetySet[0] = new TrapezoidFuzzy;
	SafetySet[1] = new TrapezoidFuzzy;

	SafetyMin[0] = 0;
	SafetyMax[0] = 30;

	SafetyMin[1] = 20;
	SafetyMax[1] = 100;

	SafetySet[0]->setInterval(SafetyMin[0], SafetyMax[0]);
	SafetySet[0]->setMiddle(0, 20);
	SafetySet[0]->setType('r');
	SafetySet[0]->setName("vengeful");

	SafetySet[1]->setInterval(SafetyMin[1], SafetyMax[1]);
	SafetySet[1]->setMiddle(30, 100);
	SafetySet[1]->setType('r');
	SafetySet[1]->setName("safe");
}

void SpawnWasp(float time)
{
	if(timer >= time)
	{
		timer = 0;
		float x = rand() % width + (-(width/2));
		float y = rand() % height + (-(height / 2));
		
		//Spawn wasp
		wasp.pos = glm::vec2(x, y);

		wasps.push_back(wasp);
	}
}

int main()
{
    // start GL context and O/S window using the GLFW helper library
    if (!glfwInit())
    {
        fprintf(stderr, "ERROR: could not start GLFW3\n");
        return 1;
    }

    window = glfwCreateWindow(width, height, "Tutorial 1 - Pong", NULL, NULL);
    if (!window)
    {
        fprintf(stderr, "ERROR: could not open window with GLFW3\n");
        glfwTerminate();
        return 1;
    }
    glfwMakeContextCurrent(window);
    glfwSetWindowSizeCallback(window, ResizeEvent);
    glfwSwapInterval(0);
	

    // start GL3W
    gl3wInit();

    // Setup ImGui binding. This is for any parameters you want to control in runtime
    ImGui_ImplGlfwGL3_Init(window, true);
    ImGui::StyleColorsLight();

    // Get version info
    const GLubyte* renderer = glGetString(GL_RENDERER); // get renderer string
    const GLubyte* version = glGetString(GL_VERSION); // version as a string
    printf("Renderer: %s\n", renderer);
    printf("OpenGL version supported %s\n", version);

    // tell GL to only draw onto a pixel if the shape is closer to the viewer
    glEnable(GL_DEPTH_TEST); // enable depth-testing
    glDepthFunc(GL_LESS); // depth-testing interprets a smaller value as "closer"

    Initialize();

    float oldTime = 0.0f;
    while (!glfwWindowShouldClose(window))
    {
        float currentTime = (float)glfwGetTime();
        float deltaTime = currentTime - oldTime;
        oldTime = currentTime;

        // update other events like input handling 
        glfwPollEvents();

        // Clear the screen
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        ImGui_ImplGlfwGL3_NewFrame();

        // Call the helper functions
        Update(deltaTime);
        Render();
        GUI();

        // Finish by drawing the GUI on top of everything
        ImGui::Render();
        glfwSwapBuffers(window);
    }

    // close GL context and any other GLFW resources
    glfwTerminate();
    ImGui_ImplGlfwGL3_Shutdown();
    Cleanup();
    return 0;
}