//#########################################
// This file was created by:			
// Marco Valdez Balderas	#:100527945	
// Aaron Macaulay			#:100492646	
// For AI for Games, Winter 2019.		
// Professor:	Dr. Fletcher Lu			
// TA:			David Arppe			    
// Date last edited: March 20, 2019	   
//#########################################

#pragma once

#include <iostream>
#include <cmath>
#include <cstring>
#include <string>

class BaseFuzzy
{
protected:
	float MinValue, MaxValue;												   // Min and Max values for membership 
	char   cType;															   // Keep track if the type is triangle or trapezoid
	std::string  sName;														   // Name of the set (very hungry, slightly hungry, etc)

public:
	BaseFuzzy()
	{};
	virtual ~BaseFuzzy()
	{

	}

	virtual void setInterval(float leftval, float rightval);				   // Set min and max values
	virtual void setMiddle(float L = 0, float R = 0) = 0;					   // Set middle (peak) values
	virtual void setType(char c);											   // Set type-tracker char (t for triangle r for trapezoid)
	virtual void setName(std::string a_name);								   // Set name of set (like very hungry, not hungry, full)
	void DrawGraph(float min, float max);									   // Draw the graph of the set to ImGui
	char getType() const; // { return cType; }								   // Get type character
	std::string getName() const;											   // Get name string
	virtual float getValue(float a_input) = 0;								   // Evaluate function
};

class TriangleFuzzy : public BaseFuzzy											// Make a triangle shape (one peak) out of a BaseFuzzy
{
private:
	float Middle;																// Middle peak

public:
	void setMiddle(float L, float R);											// Set middle (R isn't used; L will become middle)
	float getValue(float a_input);												// Evaluate function
};

class TrapezoidFuzzy : public BaseFuzzy											// Make a trapezoid shape (two peaks) out of a BaseFuzzy
{
private:
	float Left_Middle, Right_Middle;											// Two middles; left and right

public:
	void setMiddle(float L, float R);											// Set the middles
	float getValue(float a_input);												// Evaluate function
}; 