//#########################################
// This file was created by:			
// Marco Valdez Balderas	#:100527945	
// Aaron Macaulay			#:100492646	
// For AI for Games, Winter 2019.		
// Professor:	Dr. Fletcher Lu			
// TA:			David Arppe			    
// Date last edited: March 20, 2019	   
//#########################################

#include "Ant.h"


void GameObject::update(float a_deltatime)
{

}

void AntMine::update(float a_deltatime)				// update for ant mine
{
	// Do work
	if (timer >= 1.0f)
	{
		Werk += NumberOfWorkingAnts;				// Every one second each ant earns a credit for the player
		timer = 0.0f;
	}
	//std::cout << "Werk:\t" << Werk << std::endl;
	timer += a_deltatime;
}

bool Ant::choose(float a_hunger)														// Not used. 
{
	float threshold = 0.3f;

	if (a_hunger <= threshold)
	{
		float temp = (1.0f - a_hunger - (1.0 - threshold)) * 3.3f;
		float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
		if (r > temp)
			return true; // Go get food
		else
			return false; // do not get food;
	}

	return false;
}

void Ant::update(float a_deltatime)
{

}
void Ant::update(float a_deltatime, std::vector<Wasp> &list)					// Update for ants. Give it the list of all wasp
{
	std::cout << homePosition.x << ", " << homePosition.y << std::endl;

	waspIndex = 0;

	for (int i = 0; i < list.size(); i++)				// Go through wasps and choose the closest one
	{
		if (glm::distance(pos, list[i].pos) < 100)
		{
			waspIndex = i;								// store closest wasp index in waspindex
		}

	}

	// GATHER STATE. GO GET FOOD
	if (currentState == GATHER)
	{
		if (foodSource.IsThereFood())
		{
			direction = glm::normalize(foodSource.pos - pos);			// Set target direction towards food source
			//pos += direction * 100.f * a_deltatime;								// Move towards target direciton

			if (glm::distance(foodSource.pos, pos) <= 10)
			{
				currentState = RETURN;
				foodSource.GiveFood();											// If ant is close to food source, it eats, becomes full
				hunger = 100.0f;
			}
		}
		else
		{
			currentState = RETURN;
		}
	}

	// RETURN TO THE MINE STATE
	if (currentState == RETURN)
	{
		direction = glm::normalize(homePosition - pos);		// Set arget direction
		//pos += direction * 100.f * a_deltatime;

		if (glm::distance(homePosition, pos) <= 10)			// Once it gets there, start mining
		{
			currentState = MINE;
		}
	}

	// CURRENTLY MINE STATE
	if (currentState == MINE)
	{
		direction = glm::vec2(0);							// Don't go anywhere; direction blank
	}

	// WANDER STATE, WANDERING RANDOMLY
	if (currentState == WANDER)
	{

		float x = rand() % 3 + (-1);
		float y = rand() % 3 + (-1);
		direction = glm::vec2(x, y);			// Random direction
	}

	// CURRENTLY ATTACKING STATE
	if (currentState == ATTACK)
	{
		currentState = RETURN;
		if (list.size() > 0)
		{
			direction = glm::normalize(list[waspIndex].pos - pos);		// Target position is closest wasp
			if (glm::distance(pos, list[waspIndex].pos) < 25)
			{
				list[waspIndex].DamageWasp(0.5f);						// If you are super close to the wasp, damage the wasp
			}
		}
		else
			currentState = RETURN;

	}

	printState();		// Debug
	//std::cout << "Very hungry:\t" << membershipRate[0] << "\n";
	//std::cout << "Comfortable:\t" << membershipRate[1] << "\n";
	//std::cout << "Full:\t" << membershipRate[2] << "\n";

	//hunger = 100.0f - (float)((int)hungerTimer % 1000);

	for (int j = 0; j < 2; j++) // Loop through the fuzzy sets for the closest wasp
	{
		if (list.size() > 0)
		{
			safetyRate[j] = SafetySet[j]->getValue(glm::distance(pos, list[waspIndex].pos));		// Decide if the wasp is close enough to feel threatened by it
		}
	}
	
	//else if(safetyRate[0] < safetyRate[1])
	//{
	//	if (glm::distance(homePosition, pos) > glm::distance(foodSource.pos, pos))
	//	{
	//		currentState = GATHER;
	//	}
	//	else
	//		currentState = RETURN;
	//}

	for (int i = 0; i < 3; i++) // Check state of hunger
	{

		//if (HungerSet[i]->isDotInInterval(hunger))
		//	cout << "In the interval";
		//else
		//	cout << "Not in the interval";

		membershipRate[i] = HungerSet[i]->getValue(hunger);
	}

	if (membershipRate[0] > membershipRate[1]) // Ant is very hungry
	{
		std::cout << name << ": I'm very hungry! I'm getting food.\n";
		currentState = GATHER;
	}
	else // Ant is not very hungry
	{
		// Do nothing! The ant is already mining.
	}
	if (safetyRate[0] > safetyRate[1]) // Ant feels vengeful 
	{
		currentState = ATTACK;
	}

	hungerTimer += a_deltatime;
	std::cout << hungerTimer << std::endl;
	if (hungerTimer >= 1.0f)
	{
		hungerTimer = 0;
		hunger -= 1.0f;
		if (hunger <= 0.0f)
		{
			hunger = 0.0f;
			health--;
		}
	}
	pos += direction * 100.f * a_deltatime;

	timer += a_deltatime;
}


AntType Ant::type()			// get the charcater that corresponds to type of ant. not really used
{
	return myType;
}

std::string Ant::getState()		// Return the current state as a string
{
	if (currentState == GATHER)
		return "GATHER";
	if (currentState == RETURN)
		return "RETURN";
	if (currentState == WANDER)
		return "WANDER";
	if (currentState == MINE)
		return "MINE";
	if (currentState == ATTACK)
		return "ATTACK";
}

void Ant::printState()	// Used for debug
{
	std::cout << "Name: \t" << name << "\n";
	std::cout << "Hunger:\t" << hunger << "\n";
	std::cout << "Health:\t" << health << "\n";
	std::cout << "State:\t" << getState() << "\n";
}
Wasp::Wasp() // constructor
{
	float x = rand() % 3 + (-1);
	float y = rand() % 3 + (-1);
	direction = glm::vec2(x, y);
	currentState = ROAM;
}

void Wasp::update(float a_deltatime)
{
	// WILDLY ROAM
	if (currentState == ROAM)
	{
		if (timer <= directionTime)
			timer += a_deltatime;
		else
		{
			timer = 0;
			float x = rand() % 3 + (-1);
			float y = rand() % 3 + (-1);

			direction = glm::vec2(x, y); // Random direction
		}

		pos += direction * speed * a_deltatime;
		//CheckNearbyAnts();
	}
	// ATTACK STATE
	else if (currentState == ATTACK)
	{
		timer += a_deltatime;

		direction = glm::normalize(ants[antIndex].pos - pos); // target direction is the closest ant

		pos += direction * speed * a_deltatime;		// move there

		if (glm::distance(ants[antIndex].pos, pos) <= 20 && timer > attackSpeed)
		{
			timer = 0;
			ants[antIndex].health = 25.f;

			currentState = ROAM;
		}
	}
	else if (currentState == THREATENED)
	{
		currentState = ATTACK;
	}
}

void Wasp::update(float a_deltatime, std::vector<Ant>& list)
{
	if (currentState == ROAM)
	{
		if (timer <= directionTime)
			timer += a_deltatime;
		else
		{
			timer = 0;
			float x = rand() % 3 + (-1);
			float y = rand() % 3 + (-1);

			direction = glm::vec2(x, y);
		}

		pos += direction * speed * a_deltatime;
		CheckNearbyAnts(list);
	}
	else if (currentState == ATTACK)
	{
		if (list.size() > 0)
		{
			timer += a_deltatime;
			if (antIndex >= list.size() && list.size() != 0)
				antIndex = list.size() - 1;

			direction = glm::normalize(list[antIndex].pos - pos);

			pos += direction * speed * a_deltatime;
			float distance = glm::distance(list[antIndex].pos, pos);
			if (distance <= 20 && timer > attackSpeed && list[antIndex].currentState != Ant::States::MINE)
			{
				timer = 0;
				list[antIndex].health -= 25.f;

				currentState = ROAM;
			}
		}
		else
			currentState = ROAM;
	}
	else if (currentState == THREATENED)
	{
		currentState = ATTACK;
	}
}

void Wasp::CheckNearbyAnts(std::vector<Ant> &list)		// Check all nearby ants for the closest one
{
	if (list.size() > 0)
	{
		for (int i = 0; i < list.size(); i++)
		{
			if (glm::distance(pos, list[i].pos) < 100 && list[i].currentState != Ant::States::MINE)
			{
				antIndex = i;							// store index of the closest ant in the ants array
				currentState = ATTACK;					// if its close neough, attack 
				break;
			}
		}
	}
}

